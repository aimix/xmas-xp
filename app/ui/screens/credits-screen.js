import Screen from './screen';


class GameScreen extends Screen {

  constructor() {

    super( '.credits' );

    this.closeEl = this.el.querySelector('.close')

    this.closeEl.addEventListener('click', this.hide.bind(this))

  }


} 

export default GameScreen;