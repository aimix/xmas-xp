class Screen {

  constructor( selector ) {

    this.el = document.querySelector( selector );

  }

  show() {

    this.el.classList.add('show');

  }

  hide() {

    this.el.classList.remove('show');

  }

}

export default Screen;