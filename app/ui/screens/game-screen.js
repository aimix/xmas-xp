import Screen from './screen';
import Gauge from 'components/gauge';
import EventEmitter from 'lib/event-emitter';

let instructionHasBeenShowed = false

class GameScreen extends Screen {

  constructor() {

    super( '.game' );

    this.gauge = new Gauge();
    this.el.appendChild( this.gauge.el )

    this.overlayEl = this.el.querySelector('.overlay');
    this.creditsBtn = this.el.querySelector('.creditsBtn');
    this.tutoEl = this.el.querySelector('.tuto');

    this.creditsBtn.addEventListener('click', this.onCreditsBtnClicked.bind(this))

  }

  transitionInInstruction() {
    this.tutoEl.classList.add('show')
  }

  transitionOutInstruction() {
    this.tutoEl.classList.remove('show')
  }

  onCreditsBtnClicked() {

    EventEmitter.emit('CREDITS_BTN_CLICKED')

  }

  transitionInOverlay() {

  	this.overlayEl.classList.add('active')

  } 

  transitionOutOverlay() {

  	this.overlayEl.classList.remove('active')

  } 

  update( santaAngle ) {

    this.gauge.update( santaAngle );

  } 

  restrat( ) {

  	this.el.classList.remove('win');

  }	

} 

export default GameScreen;