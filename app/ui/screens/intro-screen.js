import Screen from './screen';
import EventEmitter from 'lib/event-emitter';

let base_url = './assets/img/anim_logo/';
let nbFrames = 75;
let frames = [];
let framesLoaded = 0;

let canEnter = true

class IntroScreen extends Screen {

  constructor() {

    super( '.intro' );

    this.title = this.el.querySelector('.title');
    this.startBtn = this.el.querySelector('.start-btn');
    this.overlayEl = this.el.querySelector('.overlay');

  }

  load(){

    for ( let i = 0; i < nbFrames; i++ ) {

      let img = new Image();
      img.onload = () => {
        this.onLoadProgress()
      }
      img.src = `${base_url}logo_${i}.png`
      frames.push( img );

    }

  }

  transitionInTitle() {

    let o = { frame: 0 }

    TweenMax.to( o, 2.5, { delay: 1, frame: 74, onUpdate: () => {
      this.updateFrame( o.frame )
    } } )

  }

  transitionOutTitle() {

    let o = { frame: 74 }

    TweenMax.to( o, 2.5, { frame: 0, onUpdate: () => {
      this.updateFrame( o.frame )
    } } )

  }

  updateFrame( frame ) {
    let f = Math.floor( frame )
    this.title.style.backgroundImage = `url('${base_url}logo_${f}.png')`

  }

  onLoadProgress() {

    framesLoaded++

    if ( framesLoaded === nbFrames ) {
      this.onLoadComplete()
    }

  }

  onLoadComplete() {

    EventEmitter.emit('XP_LOADED');

  }


  showStartBtn() {

    this.startBtn.classList.add('active');
    this.startBtn.addEventListener( 'click', this.onStartBtnClicked.bind(this) )
    window.addEventListener( 'keyup', ( { keyCode } ) => {
      if ( !canEnter ) return
      canEnter = false
      if ( keyCode === 32 || keyCode === 13 ) {
        this.onStartBtnClicked()
      }
    } )

  }

  onStartBtnClicked() {
    this.startBtn.classList.remove('active');
    this.startBtn.classList.add('activated');
    EventEmitter.emit( 'STARTBTN_CLICKED' )

  }

  transitionInOverlay() {

    this.overlayEl.classList.add('active')

  } 

  transitionOutOverlay() {

    this.overlayEl.classList.remove('active')

  } 


} 

export default IntroScreen;