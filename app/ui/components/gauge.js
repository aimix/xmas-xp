import NumberUtils from 'utils/number-utils';
import GUIController from 'utils/gui-controller'

let canvas;
let ctx;
let label;
let labelTxt;

const width = 140;
const height = 140;
const minAngle = NumberUtils.toRadians( 220 )
const maxAngle = NumberUtils.toRadians( 320 )

let isOutofBounds = false;
let warningAlpha = 0;

class Gauge {

  constructor() {

    this.isVisible = true

    this.el = document.createElement('div');
    this.el.className = 'gauge';

    canvas = document.createElement('canvas');
    canvas.width = canvas.height = 140
    ctx = canvas.getContext('2d');

    label = document.createElement('span');
    label.className = 'label';
    label.innerText = 'BALANCE';

    this.el.appendChild( canvas );
    this.el.appendChild( label );
    
    // this.initGUIController()

    this.toggleVisible()


  }

  initGUIController() {

    let gaugeFolder = GUIController.gui.addFolder( 'Gauge' );

    // Gauge options
    // -------------------------------
    gaugeFolder.add( this, 'toggleVisible' ).name('toggle visibility');

  }

  toggleVisible() {

    this.isVisible = !this.isVisible

    if ( this.isVisible ) {
      this.el.classList.add('active')
    }
    else {
      this.el.classList.remove('active')
    }

  }

  update( value ) {

    let angle = NumberUtils.map( value, 0.3, -0.3, minAngle, maxAngle );
    if ( angle < minAngle || angle > maxAngle ) {

      if ( angle < minAngle ) angle = minAngle;
      if ( angle > maxAngle ) angle = maxAngle;

      if ( warningAlpha < .6 ) warningAlpha += 0.08

    }
    else {

      if ( warningAlpha > 0 ) warningAlpha = 0

    }
    

    // clear
    ctx.clearRect(0, 0, width, height);

    //range
    ctx.globalAlpha = 1;
    ctx.beginPath();
    ctx.arc(width / 2, height / 2, width * 0.35, minAngle, maxAngle, false);
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#fff';
    ctx.stroke();
    
    // indicator
    const x = (width/2) + Math.cos( angle ) * ( width * 0.35 );
    const y = (width/2) + Math.sin( angle ) * ( width * 0.35 );
    ctx.globalAlpha = 1;
    ctx.beginPath();
    ctx.arc( x, y, 4, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'white';
    ctx.fill();

    // red
    const xx = (width/2) + Math.cos( angle ) * ( width * 0.35 );
    const yy = (width/2) + Math.sin( angle ) * ( width * 0.35 );
    ctx.globalAlpha = 1;
    ctx.beginPath();
    ctx.arc( xx, yy, 4, 0, 2 * Math.PI, false);
    ctx.fillStyle = `rgba(255,0,0,${warningAlpha})`;
    ctx.fill();

    // stroke
    const xxx = (width/2) + Math.cos( angle ) * ( width * 0.35 );
    const yyy = (width/2) + Math.sin( angle ) * ( width * 0.35 );
    ctx.globalAlpha = 1;
    ctx.beginPath();
    ctx.arc( x, y, 4.2, 0, 2 * Math.PI, false);
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#fff';
    ctx.stroke();

  } 

}

export default Gauge;  