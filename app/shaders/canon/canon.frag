varying vec2 vUv;
uniform sampler2D diffuse;
uniform float alpha;

void main() {
  vec3 diffuseTexture = texture2D( diffuse, vUv ).xyz;
  gl_FragColor = vec4( diffuseTexture, alpha );
}