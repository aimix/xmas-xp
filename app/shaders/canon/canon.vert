uniform float displacement;
uniform float amplitude;
varying vec2 vUv;

const float bottom = -0.4865;
const float top = -1.2243;
const float PI = 3.14159265359;

void main() {
	vUv = uv;
	float coef = max(0.0,amplitude*sin(((position.z-bottom)/(top-bottom)-displacement*2.0-1.0)*PI));
	vec3 newPosition = position + normal * vec3(coef ) * vec3(color.g - color.r);
	gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );
}
