#pragma glslify: map = require(glsl-map)

uniform float santaPos;
varying vec2 vUv;
varying vec3 vPos;
varying float vy;

void main() {
  
  float dist = distance( uv, vec2(0.5, 0.5) );

  vec3 newPosition = position;
  
  // if ( dist < .1 ) {
  //   newPosition.z = map( dist, 0.0, 0.1, -5.0, 0.0);  
  // }

  newPosition.z -= 10.0 * exp( -dist * dist );

  // newPosition.z = uv.y * 100.0;
  

  gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );

  vUv = uv;
  vPos = position;
  vy = newPosition.z / 100.0;

}
