#pragma glslify: map = require(glsl-map)

uniform float santaPos;
varying vec2 vUv;
varying vec3 vPos;
varying float vy;

void main() {

  float dist = distance( vUv.x, 0.5 );

  float alpha = 0.0;
  
  if ( dist < .02 ) {
    alpha = map( dist, 0.0, 0.02, 1.0, 0.0);  
    // newPosition.x = exp( -uv.x * uv.x );
  }

  gl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );

}