#pragma glslify: backOut = require(glsl-easings/back-out) 

uniform float time;
varying vec2 vUv;
varying vec3 vPosition;

float linearTween( float t, float b, float c, float d ) {
  return c * t / d + b;
}

float backIn( float t, float b, float c, float d ) {
  float s = 1.70158;
  return c*(t/=d)*t*((s+1.0)*t - s) + b;
}

float rand(vec2 co){

  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);

}

void main() {

  vec3 newPosition = position;

  float t = min( uv.y - time, 0.0 );

  if ( position.z > -9.75 ) {
    newPosition.y = linearTween( t, position.y, position.y + 5.0, 1.0 );    
  }
  
  
  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

  vUv = uv;
  vPosition = position;

}