varying vec2 vUv;
varying vec3 vPosition;
uniform sampler2D diffuse;

uniform float time;

void main() {

  float alpha = 1.0;
  if ( vPosition.z > -9.75 ) {
    alpha = 1.0 - time;
    // alpha = 0.0;
  }

  vec3 diffuseTexture = texture2D( diffuse, vUv ).xyz;
  gl_FragColor = vec4( diffuseTexture, alpha );

}