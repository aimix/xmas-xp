#pragma glslify: map = require(glsl-map)

uniform float time;
uniform vec2 mouse;
uniform vec2 u_resolution;
uniform vec3 firstColor;
uniform vec3 secondColor;
uniform float stripeSpeed;
uniform float nbWaves;

uniform sampler2D sea_lut;
const float width = 30.0;

#define LIGHT_DIRECTION vec3( -0., 0.0, -1.0 )

varying vec2 vUv;
// varying float light;
varying vec3 nrml;


void main( void ) {

    /*  WAVE
     --------------- */

    float r = ( length(vUv-0.5) * nbWaves * .1 );



    /*  LIGHTING
     --------------- */

    float light = max( dot( (nrml), normalize(LIGHT_DIRECTION) ), 0.0 );


    vec3 color = firstColor * texture2D( sea_lut, vec2( .5, r ) ).rgb;
    // if ( r <= 0.5 ) {
    //   color = vec3( firstColor.r / 255.0, firstColor.g / 255.0, firstColor.b / 255.0 );
    // }
    // else {
    //  color = vec3( secondColor.r / 255.0, secondColor.g / 255.0, secondColor.b / 255.0 );
    // }

    color *= pow(light, 4.0);

    float dist = distance( vUv, vec2(0.5, 0.5) );



    /*  FAKE FOG
     --------------- */

    float alpha = 1.0;
    if ( dist > 0.3 ) {
      alpha = map( dist , 0.3, 0.5, 1.0, 0.0);
    }




    gl_FragColor = vec4( color * alpha, 1.0 );

}
