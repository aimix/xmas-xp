#pragma glslify: snoise2 = require(glsl-noise/simplex/2d) 

uniform float time;
uniform float amplitude;
uniform float holeDepth;
uniform float holeWidth;
uniform float speed;

varying vec2 vUv;
varying vec3 newPos;
varying float noise;
varying float light;
varying vec3 nrml;


#define EPSILON 0.01

float calcHeight( vec2 uv ) {

  
  float frequence = 5.0;
  float displacement = snoise2( frequence*uv.xy + time * speed);
  float height = displacement * amplitude;

  float dist = -holeWidth * distance( uv, vec2(0.5, 0.5) );
  
  height -= holeDepth * exp( -dist * dist );

  return height;

}

void main() {

    vUv = uv;

  
    vec3 newPosition = position;    
    newPosition.z = calcHeight( vUv );

    float dz = calcHeight( vUv + vec2( 0.0, EPSILON ) );
    vec3 zx = vec3( 0.0, EPSILON * 800., dz - newPosition.z );

    dz = calcHeight( vUv + vec2( EPSILON, 0.0 ) );
    vec3 zy = vec3( EPSILON * 800., 0.0, dz - newPosition.z );

    nrml = normalize( cross( zx, zy ) );


    gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );

    newPos = newPosition;

}