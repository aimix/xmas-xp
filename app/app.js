import Scene from './scene/scene'
import Santa from 'entities/characters/santa'
import AppController from 'controllers/app-controller';

class App {

  constructor() {
    this.SPEED = 1
    this.SPACE_DOWN = false

    this.DELTA_TIME = 0
    this.LAST_TIME = Date.now()
    this.START_TIME = Date.now()

    this.width = window.innerWidth
    this.height = window.innerHeight
    this.nextAnimation=""

    this.appController = new AppController();

    this.effectController = {
      'speed': this.SPEED
    };
    this.currentTile = 0
    this._binds = {}
    this.addListeners()
  }

  addListeners() {
    window.addEventListener( 'resize', this.onResize.bind(this) )
    TweenMax.ticker.addEventListener( 'tick', this.update.bind(this) )
  }

  addRope() {
    Scene.add( Rope )
  }

  addSea() {
    Sea.init()
    Scene.add( Sea )
  }

  addLevel() {
    Scene.add( Level )
  }

  addSanta() {
    Scene.add( Santa.container )
    Santa.container.position.x = 100
    Santa.container.position.y = -1.4
  }

update() {

    this.DELTA_TIME = Date.now() - this.LAST_TIME
    this.LAST_TIME = Date.now()

    this.appController.update( this.DELTA_TIME );

  }

  onResize( evt ) {
    this.width = window.innerWidth
    this.height = window.innerHeight
    Scene.resize( this.width, this.height )
  }

}

export default App
