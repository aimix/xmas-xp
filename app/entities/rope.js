import NumberUtils from 'utils/number-utils';


class Rope extends THREE.Mesh {

	constructor() {

    let geo = new THREE.PlaneGeometry( 400, 1, 300, 300 ); 
    let mat = new THREE.MeshBasicMaterial( { color: '0xff0000' } )

    super( geo, mat ); 

  }

  update( santaPos ) {

    
    this.rotation.x = -Math.PI / 2;

    for ( let i = 0; i < this.geometry.vertices.length; i++ ) {

      let v = this.geometry.vertices[i]

      const minDist = 5;
      const dist = this.geometry.vertices[i].distanceTo( santaPos )

      if ( dist < minDist ) {
        const z = NumberUtils.map( dist, minDist, 0, 0, -2 )
        v.z = z;
      }
      else {
        v.z = 0;
      }

    }

    this.geometry.verticesNeedUpdate = true;

  }

}

const _r = new Rope();

export default _r;