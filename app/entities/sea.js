import Scene from 'scene';
import GUIController from 'utils/gui-controller'

const glslify = require('glslify');

class Sea extends THREE.Object3D {

  constructor() {

    super();

  }

  init() {

    var lutTex = THREE.ImageUtils.loadTexture('assets/models/sea_lut.jpg');
    // lutTex.generateMipmaps = true
    lutTex.minFilter = THREE.Linear;
    lutTex.magFilter = THREE.Linear;
    lutTex.wrapS =THREE.RepeatWrapping
    lutTex.wrapT =THREE.RepeatWrapping

    this.uniforms = {
      time: { type: "f", value: 0.0 },
      firstColor: { type: 'v3', value: new THREE.Vector3( 1.0, 1.0, 1.0 ) },
      secondColor: { type: 'v3', value: new THREE.Vector3( 0.0, 0.0, 0.0 ) },
      amplitude: { type: 'f', value: 8.0 },
      speed: { type: 'f', value: 1 },
      stripeSpeed: { type: 'f', value: 0.05 },
      nbWaves: { type: 'f', value: 500 },
      holeDepth: { type: 'f', value: 800 },
      holeWidth: { type: 'f', value: 10 },
      sea_lut:{ type: "t", value:lutTex  }
    }

    let geo = new THREE.PlaneBufferGeometry( 800, 800, 400, 400 )
    let material = new THREE.ShaderMaterial( {
      uniforms: this.uniforms,
      vertexShader: glslify('../shaders/sea/sea.vert'),
      fragmentShader: glslify('../shaders/sea/sea.frag')
     } );

    this.colorLayer = new THREE.Mesh( geo, material )

    this.rotation.x = -Math.PI/2
    this.position.y = -60

    this.add( this.colorLayer )

    // this.initGUIOptions()

  }

  initGUIOptions() {

    const seaFolder = GUIController.gui.addFolder( 'Sea' );

    // first color options
    // -------------------------------
    const firstColorFolder = seaFolder.addFolder( 'First color' )
    firstColorFolder.add( this.colorLayer.material.uniforms.firstColor.value, 'x', 0, 255  ).name('r');
    firstColorFolder.add( this.colorLayer.material.uniforms.firstColor.value, 'y', 0, 255  ).name('v');
    firstColorFolder.add( this.colorLayer.material.uniforms.firstColor.value, 'z', 0, 255  ).name('b');
    firstColorFolder.open()

    // second color options
    // -------------------------------
    const secondColorFolder = seaFolder.addFolder( 'Second color' )
    secondColorFolder.add( this.colorLayer.material.uniforms.secondColor.value, 'x', 0, 255  ).name('r');
    secondColorFolder.add( this.colorLayer.material.uniforms.secondColor.value, 'y', 0, 255  ).name('v');
    secondColorFolder.add( this.colorLayer.material.uniforms.secondColor.value, 'z', 0, 255  ).name('b');
    secondColorFolder.open()

    // Wave options
    const waveFolder = seaFolder.addFolder( 'Wave' )
    waveFolder.add( this.colorLayer.material.uniforms.amplitude, 'value', 0.0, 10.0  ).name('amplitude');
    waveFolder.add( this.colorLayer.material.uniforms.speed, 'value', 0.0, 5.0  ).name('speed').step(0.01);
    waveFolder.add( this.colorLayer.material.uniforms.stripeSpeed, 'value', 0.0, 0.300  ).name('stripe speed').step(0.01);
    waveFolder.add( this.colorLayer.material.uniforms.nbWaves, 'value', 0.0, 1000.0  ).name('nbWaves').step(0.01);
    waveFolder.open()

     // Hole options
    const holeFolder = seaFolder.addFolder( 'Hole' )
    holeFolder.add( this.colorLayer.material.uniforms.holeDepth, 'value', 0.0, 1000.0  ).name('depth');
    holeFolder.add( this.colorLayer.material.uniforms.holeWidth, 'value', 0.0, 50.0  ).name('width').step(0.01);
    holeFolder.open()


  }

  goNormal() {

    TweenMax.to( this.colorLayer.material.uniforms.firstColor.value, .3, { x: 1, y: 1, z: 1 } )
    TweenMax.to( this.colorLayer.material.uniforms.amplitude, .5, { value: 6 } )
    // TweenMax.to( this.colorLayer.material.uniforms.speed, .5, { value: 0.05 } )

  }

  goWarning() {

    TweenMax.to( this.colorLayer.material.uniforms.firstColor.value, 2, { x: 1, y: 0, z: 0 } )
    TweenMax.to( this.colorLayer.material.uniforms.amplitude, 2, { value: 15 } )
    // TweenMax.to( this.colorLayer.material.uniforms.speed, .3, { value: 0.01 } )

  }

  transitionOut() {

    TweenMax.to( this.colorLayer.material.uniforms.holeWidth, 5, { value: 0 } )

  }

  update( dt ) {

    this.colorLayer.material.uniforms[ 'time' ].value += .00025 * dt;

    // this.colorLayer.material.uniforms[ 'firstColor' ].value.x = this.opts.firstColor

  }

}

const _sea = new Sea()

export default _sea