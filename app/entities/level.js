import level_geo from "data/level_geo.json"
import GUIController from 'utils/gui-controller'
const glslify = require('glslify');

class Level extends THREE.Mesh {
  constructor() {

    let loader = new THREE.JSONLoader()
    let geo = loader.parse(level_geo)

    let uniforms = { 
      diffuse: { type: "t", value: THREE.ImageUtils.loadTexture('assets/models/level.jpg') },
      alpha: { type: "f", value: 1.0 },
      time: { type: "f", value: 0.0 }
    }

    let material = new THREE.ShaderMaterial( {
      uniforms: uniforms,
      vertexColors: THREE.VertexColors,
      vertexShader: glslify('../shaders/level/level.vert'),
      fragmentShader: glslify('../shaders/level/level.frag'),
      transparent: true
    } );

    super(geo.geometry, material );

    this.scale.set(20,20,20);
    this.rotation.y = Math.PI/2

    // this.initGUIOptions();

  }

  initGUIOptions() {

    let levelFolder = GUIController.gui.addFolder( 'Level' );

    // Gauge options
    // -------------------------------
    levelFolder.add( this.material.uniforms.time, 'value', 0.0, 1.0 );


  }

  transitionOut() {

    TweenMax.to( this.material.uniforms.time, 1, { value: 1, ease: Linear.None } )

  }

}

const _l = new Level();

export default _l;