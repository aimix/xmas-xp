import projectile_geo from "data/projectile_geo.json"

class Projectile extends THREE.Mesh {

	constructor() {
		let loader = new THREE.JSONLoader()
		let geo = loader.parse(projectile_geo)
		let material = new THREE.MeshBasicMaterial({map:THREE.ImageUtils.loadTexture('assets/models/projectile.jpg'),color:0xe0e0e0, transparent: true})
		super(geo.geometry, material );
		this.scale.set(25,25,25);
		this.rotation.y = Math.PI/2
	}
}

export default Projectile;