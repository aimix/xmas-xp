import EventEmitter from 'lib/event-emitter';
import GUIController from 'utils/gui-controller';
import NumberUtils from 'utils/number-utils';
import SoundManager from 'managers/sound-manager';

const config = {
  baseUrl: "assets/models/santa/",
  body: "santa.md2",
  skins: [ "santa.jpg" ],
  weapons:  [  ],
  animations: {
    idle:"idle",
    idle_ballance:"idleb",
    idle_ballance_right:"idlebr",
    idle_ballance_left:"idlebl",
    walk_ballance:"walkb",
    walk:"walk",
    win:"win"
  }
}

const opts = {
  affectedByCollision: true,
  minAnimationFPS: 9,
  maxAnimationFPS: 15,
  minSpeed: .03,
  maxSpeed: .15
  // maxSpeed: 4
}

let angleAcceleration = 0;
let angleVelocity = 0;

let angleGravity = 0;

let minAnimationFPS = 9;
let maxAnimationFPS = 15;
let minSpeed = .03
let maxSpeed = .3
let unbalancedAnimFPS = 6;

let timer;
let isAboutToFall = false;

let hasWin = false

let voidVector = new THREE.Vector3( 0, -10, 0 )

let timeUnbalanced = 0

class Santa extends THREE.MD2CharacterComplex {

  constructor() {
    
    super()

    let controls = {
      moveForward: false,
      moveBackward: false,
      moveLeft: false,
      moveRight: false
    };
    

    // this.scale = 1
    this._currentAnimation = config.animations.walk_ballance
    this.spped = 2
    this.animationFPS = 6
    // this.animationFPS = 25
    this.transitionFrames = 25
    this.controls = controls
    // this.transitionFrames = 25;

    // this.onLoadComplete = this.onCharacterLoad.bind(this)

    this.hasStarted = false

    this.scale = 20
    this.loadParts( config )
    this.root.position.x = 0
    this.root.position.z = 0

    this.container = new THREE.Group()
    this.container.rotation.y = Math.PI * 1.5
    this.container.add(this.root)

    this.binds = {}
    this.binds.setAnimation = this._setAnimation.bind(this)

    window.addEventListener( 'keydown', this.onKeyDown.bind(this) )
    // window.addEventListener( 'keyup', this.onKeyUp.bind(this) )


    EventEmitter.on( 'PROJECTILE_ON_TARGET', this.onProjectileCollided.bind(this) )

    // this.initGUIOptions()
    

  }

  init() {

  }

  resett() {

    this.binds.setAnimation( config.animations.idle )

    this.hasWin = false
    this.hasStarted = false
    this.isDead = false
    this.container.rotation.x = 0;

    angleAcceleration = 0
    angleVelocity = 0
    

  }

  initGUIOptions(){

    let santaFolder = GUIController.gui.addFolder( 'Santa' );

    // Collision options
    // -------------------------------
    const collisionFolder = santaFolder.addFolder( 'Collision' )
    collisionFolder.add( opts, 'affectedByCollision' ).name('collision');
    

    // FPS options
    // -------------------------------
    const fpsFolder = santaFolder.addFolder( 'Anim speed' )
    fpsFolder.add( opts, 'minAnimationFPS', 0, 20 ).name('min');
    fpsFolder.add( opts, 'maxAnimationFPS', 0, 20 ).name('max');

    // Walk options
    // -------------------------------
    const walkerFolder = santaFolder.addFolder( 'Walk speed' )
    walkerFolder.add( opts, 'minSpeed', 0, 2 ).name('min');
    walkerFolder.add( opts, 'maxSpeed', 0, 2 ).name('max');

  } 

  /**
   * Called when character is fuly loaded
   */
  onLoadComplete() {
    
    this.setSkin( 0 )
    this.binds.setAnimation( config.animations.idle )
    

  }

  _setAnimation( animationName ) {

    if (this._currentAnimation!=animationName){
      this.setAnimation( animationName );
      this._currentAnimation=animationName;
    }

  }

  onKeyDown( { keyCode } ) {

    if ( keyCode === 37 ) {
      angleVelocity += .001
    }
    else if ( keyCode === 39 ) {
      angleVelocity -= .001
    }

  }

  onKeyUp( { keyCode } ) {

    if ( keyCode === 37 ) {
      angleVelocity += .001
    }
    else if ( keyCode === 39 ) {
      angleVelocity -= .001
    }

  }


  onProjectileCollided( side ) {

    if ( !opts.affectedByCollision || this.isDead ) return

    if ( Math.random() < .4 ) {
      SoundManager.play('ouch_echo')
    }
    

    // if about to fall
    if ( isAboutToFall ) {
      
      this.fall()

    }
    // not falling ?
    else {

      // from the left
      if ( side === 1) {
        angleVelocity -= .0015
      }
      // from the right
      else {
        angleVelocity += .0015
      }

    }
    

  }

  startPosition() {
    this.container.position.x = 221
    this.container.position.y = .5
  }
  enterTheRope() {

    let entered = false

    this.binds.setAnimation( config.animations.walk )
    TweenMax.to( this.container.position, .5, {delay:2.3,ease:Linear.easeNone, y: -1.4})
    TweenMax.to( this.container.position, 2.5, {ease:Power1.easeIn, x: 192, onComplete: () => {
      this.binds.setAnimation( config.animations.walk_ballance )
      TweenMax.to( this.container.position, .7, {x: 190})
      entered = true
      this.enterTheRopeComplete()
    }} )
  }

  enterTheRopeComplete() {

    EventEmitter.emit('SANTA_ENTERED_THE_ROPE')
    this.binds.setAnimation( config.animations.idle_ballance )

  }

  start() {

    this.hasStarted = true
    this.binds.setAnimation( config.animations.walk_ballance )

  }

  fall() {

    EventEmitter.emit('SANTA_DIED');

    // SoundManager.play('fall_fade')

    this.isDead = true

  }

  win() {

    this.hasWin = true

    this.binds.setAnimation( config.animations.walk )
    TweenMax.to( this.container.position, 0.7, { y: .5})
    TweenMax.to( this.container.rotation, 4, { x: 0})
    TweenMax.to( this.container.position, 4, { 
      x: -220,
      onComplete: () => {
        this.binds.setAnimation( config.animations.win )    
      }
    } )

    

  }

  uupdate( delta ) {

    this.update( delta );

    if ( this.hasWin || this.isDead || !this.hasStarted ) return 


    // always walk animation by default
    // ---------------------------------------------------
    let nextAnimation = this._currentAnimation
    nextAnimation = config.animations.walk_ballance


    // automatic walk if well balanced
    // ---------------------------------------------------
    if ( !this.isUnbalanced ) {

      let angle = Math.abs( this.container.rotation.x )
      this.speed = NumberUtils.map( angle, .3, 0, opts.minSpeed, opts.maxSpeed )
      let fps = NumberUtils.map( angle, .3, 0, opts.minAnimationFPS, opts.maxAnimationFPS )
      this.container.position.x -= this.speed
      this.meshBody.setAnimationFPS( "walkb", fps )

      isAboutToFall = false

    }
    else if ( this.meshBody && this.isUnbalanced ) {

      isAboutToFall = true    

    }


    // """"""" physics """""""
    // ---------------------------------------------------

    if ( Math.abs( this.container.rotation.x ) < .3 ) {
      let amount = NumberUtils.map( Math.abs( this.container.rotation.x ), 0, .3, .00003, .00006 )
      if ( this.container.rotation.x < 0 ) angleGravity = -amount
      else angleGravity = amount
    }

    angleVelocity += angleGravity

    this.container.rotation.x += angleVelocity
    


    // dont walk if will fall
    // ---------------------------------------------------
    if ( Math.abs( this.container.rotation.x ) > .15 ) {
      // Notify once santa is unbalanced
      nextAnimation = config.animations.idle_ballance
      this.controls.moveForward = false
      // close to the edge
      if ( Math.abs( this.container.rotation.x ) > 0.25 ) {
        timeUnbalanced++

        if ( timeUnbalanced > 200 ) {
          this.fall()
        }

        if ( !this.isUnbalanced ) { EventEmitter.emit( 'SANTA_UNBALANCED' ); }
        this.isUnbalanced = true
        clearTimeout( timer )
        this.controls.moveForward = false
        
        // left
        if ( this.container.rotation.x > 0 ) {
          nextAnimation = config.animations.idle_ballance_right 
        }
        //right
        else {
          nextAnimation = config.animations.idle_ballance_left
        }
      }
    }
    else {

      if ( this.isUnbalanced ){
        clearTimeout( timer )
        timer = setTimeout( () => {
          this.controls.moveForward = true  
          nextAnimation = config.animations.walk_ballance
        }, 500 )
      } 
      // Notify once santa is back on feet
      if ( this.isUnbalanced ) {
        EventEmitter.emit( 'SANTA_BALANCED' )
        timeUnbalanced = 0
      }
      this.isUnbalanced = false
      

    }

    if (this.container.rotation.x < -0.31 ) {
      this.container.rotation.x = -0.31
    }

    if (this.container.rotation.x > 0.31 ) {
      this.container.rotation.x = 0.31
    }
    
  

    this.binds.setAnimation( nextAnimation )

  }

}

const _santa = new Santa()


export default _santa