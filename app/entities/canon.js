import canon_geo from "data/canon_geo.json"
const glslify = require('glslify');

class Canon extends THREE.Mesh {

  constructor() {

    let loader = new THREE.JSONLoader();
    let geo = loader.parse(canon_geo);
    let uniforms = { 
      diffuse: { type: "t", value: THREE.ImageUtils.loadTexture('assets/models/level.jpg') },
      displacement: { type: "f", value: 0.0 },
      amplitude: { type: "f", value: 0.25 },
      alpha: { type: "f", value: 1.0 },
    }
    let material = new THREE.ShaderMaterial( {
      uniforms: uniforms,
      vertexColors: THREE.VertexColors,
      vertexShader: glslify('../shaders/canon/canon.vert'),
      fragmentShader: glslify('../shaders/canon/canon.frag'),
      transparent: true
    } );

    super( geo.geometry, material );

    this.scale.set(12,12,12);
    this.rotation.y = Math.PI/2

  }

  transitionOut() {
    TweenMax.to( this.material.uniforms.alpha, 1, { value: 0 } )
  }

}

export default Canon;