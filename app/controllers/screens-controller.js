import IntroScreen from 'ui/screens/intro-screen';
import GameScreen from 'ui/screens/game-screen';
import CreditsScreen from 'ui/screens/credits-screen';

class ScreensController {

  constructor() {

    this.screens = {
      intro: new IntroScreen(),
      game: new GameScreen(),
      credits: new CreditsScreen()
    }

    this.currentScreen = null;

  }

  goToScreen( screenId ) {

    if ( this.currentScreen ) {
      this.currentScreen.hide()
    }

    this.currentScreen = this.screens[ screenId ];
    this.currentScreen.show()


  }

  update( { santaRotation } ) {

    this.currentScreen.update( santaRotation )

  }

}

const _sc = new ScreensController();

export default _sc;