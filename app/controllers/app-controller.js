import SoundManager from 'managers/sound-manager';
import GameController from 'controllers/game-controller';
import ScreensController from 'controllers/screens-controller';
import EventEmitter from 'lib/event-emitter';

const APP_STATE = {

  LOADING: 'loading',
  INTRO: 'intro',
  GAME: 'game',
  CREDITS: 'credits'

}

class AppController {

  constructor() {

    this.setState( APP_STATE.INTRO )
    ScreensController.screens.intro.load()

    EventEmitter.on('XP_LOADED', this.onXPLoaded.bind(this));
    EventEmitter.on( 'STARTBTN_CLICKED', this.startXP.bind(this) )
    EventEmitter.on('SANTA_ENTERED_THE_ROPE', this.onSantaEnteredTheRope.bind(this))

    EventEmitter.on( 'SANTA_DIED', this.loose.bind(this) );
    EventEmitter.on('SANTA_WON', this.won.bind(this));
    EventEmitter.on('CREDITS_BTN_CLICKED', this.onCreditsBtnClicked.bind(this))

  
  }

  onXPLoaded() {
    
    SoundManager.play('loop')

    ScreensController.screens.intro.transitionInTitle()
    ScreensController.screens.intro.transitionOutOverlay()

    this.setState( APP_STATE.GAME )

    ScreensController.screens.intro.show()
    ScreensController.screens.game.transitionOutOverlay()
    GameController.transitionInGame()

    TweenMax.delayedCall( 5, () => {
      ScreensController.screens.intro.showStartBtn()

    } )

  }

  startXP() {

    ScreensController.screens.intro.transitionOutTitle()

    TweenMax.delayedCall( 1, () => {
      ScreensController.screens.intro.hide()  

      ScreensController.screens.game.transitionInInstruction()  
      TweenMax.delayedCall( 4.5, () => {
        ScreensController.screens.game.transitionOutInstruction()  
      } )
    } )

    GameController.introduceGame();

  }

  onCreditsBtnClicked() {

    ScreensController.screens.credits.show()

  }

  won() {

    ScreensController.screens.game.gauge.toggleVisible()

  }

  loose() {

    // stop sound loop
    SoundManager.stop( 'loop' )

    // all gray
    let el = document.querySelector('.app')
    el.classList.add('gray')

    // play the HE FUCKED UP sound
    SoundManager.play( 'hefuckedup', 1 )

    // pause game
    GameController.pause()

    // hide UI
    ScreensController.screens.game.gauge.toggleVisible()

    TweenMax.delayedCall( 5, () => {
      SoundManager.play( 'fall_fade' );
      ScreensController.screens.game.transitionInOverlay();

      TweenMax.delayedCall( 1.5, this.resetGame.bind(this) )

    } )
    

  } 

  resetGame() {

    GameController.reset()
    ScreensController.screens.game.transitionOutOverlay()

    // stop sound loop
    SoundManager.play( 'loop' )

    // all gray
    let el = document.querySelector('.app')
    el.classList.remove('gray')

  }

  onSantaEnteredTheRope() {

    ScreensController.screens.game.gauge.toggleVisible();

    TweenMax.delayedCall( 1, () => {
      GameController.startGame()
    } )

  }

  setState( newState ) {

    this.state = newState;
    ScreensController.goToScreen( newState.toLowerCase() )

  }

  update( dt ) {

    if ( this.state === APP_STATE.GAME ) {


      // add what you want to pass between controllers here
      let payload = {
        santaRotation: GameController.santa.container.rotation.x
      }

      
      ScreensController.update( payload )
      GameController.update( dt )

    }

  }

}

export default AppController;