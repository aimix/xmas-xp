import Scene from 'scene';
import Level from 'entities/level';
import ProjectilesManager from 'managers/projectiles-manager';
import Santa from 'entities/characters/santa';
import Sea from 'entities/sea';
import EventEmitter from 'lib/event-emitter';
import NumberUtils from 'utils/number-utils';

let currentTime = 0;
let throwProjectileInterval = 2000;

let progression = 0;

class GameController {

  constructor() {

    Scene.init()

    let root = document.body.querySelector( '.app' )
    root.appendChild( Scene.renderer.domElement )


    this.canUpdate = false

    this.addLevel();
    this.addSanta();
    this.addSea();
    this.addProjectiles();


    Scene.camera.zoom = 9
    Scene.camera.fov = 145
    Scene.camera.updateProjectionMatrix();
    this.targetDelta = new THREE.Vector3(0,0,0);

    this.canUpdate = true

        
    EventEmitter.on( 'SANTA_BALANCED', () => {
      Sea.goNormal();
    } )

    EventEmitter.on( 'SANTA_UNBALANCED', () => {
      Sea.goWarning();
    } )


  }

  transitionInGame() {

    TweenMax.set(Scene.camera.position,{x:0, y:2,z:-30})
    TweenMax.to(Scene.camera.position,6.0,{delay:0, ease:Expo.easeInOut, x:-50, y:200,z:-300})
    TweenMax.to(Scene.camera,6.0,{delay:0, ease:Expo.easeInOut, fov:45, zoom:Scene.cameraZoom, onUpdate:()=>{
      Scene.camera.updateProjectionMatrix()
    }, onComplete:()=>{
      // this.throwProjectile()
    }})

  }

  introduceGame() {

    Santa.enterTheRope();

  }

  startGame() {


    Santa.hasStarted = true
    this.throwProjectile()

  }

  reset() {

    Santa.resett()
    Santa.startPosition()
    Sea.goNormal()

    Santa.enterTheRope()

    this.canUpdate = true

  }

  pause() {

    this.canUpdate = false

  }


  throwProjectile() {


    if ( Santa.hasWin || Santa.isDead ) return

    if ( progression > .2 ) {

      ProjectilesManager.currentTargetPosition = Santa.container.position.clone();
      if (Santa.controls.moveForward) {
        ProjectilesManager.currentTargetPosition.x//-=10;
      };


      ProjectilesManager.throwProjectile();

    }
    
    
    let throwProjectileIntervalTime = ( Math.random() * ( 2000 * (1 - progression) ) ) + 800
    throwProjectileInterval = setTimeout(this.throwProjectile.bind(this), throwProjectileIntervalTime)

  }

  addSea() {
    
    Sea.init()
    Scene.add( Sea )

  }

  addLevel() {

    Scene.add( Level )

  }


  addProjectiles() {

    ProjectilesManager.initPool()
    Scene.add( ProjectilesManager.container )

  }

  addSanta() {
    
    Scene.add( Santa.container )

    Santa.startPosition()

  }

  end() {

    ProjectilesManager.transitionOutCanons()

    TweenMax.to(this.targetDelta,6.0,{ease:Expo.easeInOut, x:17, y:-28,z:-3})
    TweenMax.to(Scene.camera,6.0,{ease:Expo.easeInOut, zoom:1.6, onUpdate:()=>{
      Scene.camera.updateProjectionMatrix()
    }})
    TweenMax.to(Scene.camera.position,6.0,{ease:Expo.easeInOut, x:-411, y:18,z:-37})

  }

  update( dt ) {

    if ( !this.canUpdate ) return

    Scene.target.x = Santa.container.position.x+this.targetDelta.x;
    Scene.target.y = Santa.container.position.y+20+this.targetDelta.y;
    Scene.target.z = Santa.container.position.z+this.targetDelta.z;

    ProjectilesManager.update( dt )
    Sea.update( dt )
    Santa.uupdate( dt / 1000 )
    ProjectilesManager.update( dt )
    Scene.render()

    progression = NumberUtils.map( Santa.container.position.x, 200, -192, 0, 1  );

    if ( progression >= 1 && !Santa.hasWin ) {

      Santa.win()
      EventEmitter.emit('SANTA_WON');
      
      setTimeout( () => {
        this.end()
      }, 100 )
      
    }


  }

  get santa() { return Santa }

}

const _gc = new GameController();

export default _gc;