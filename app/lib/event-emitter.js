/*
emitter.js
 */

'use strict';

import Emitter from 'event-emitter';

const emitter = Emitter({});

export default emitter;