const manifest = [
  'cannon',
  'fall_fade',
  'impact_round',
  'ouch_echo',
  'hefuckedup'
]

const sounds = {}
let soundsLoaded = 0;

class SoundManager {

  constructor() {

    this.load();

  }

  load() {

    let loopSound = new Howl({
      urls: [ `./assets/audio/loop_atmosphere.mp3` ],
      loop: true,
      volume: .5,
      onload: this.onSoundLoaded.bind(this)
    });
    sounds['loop'] = loopSound;

    for ( let i = 0; i < manifest.length; i++ ) {

      let s = ( manifest[i] === 'hefuckedup' ) ? 1 : .35;

      let sound = new Howl({
        urls: [ `./assets/audio/${manifest[i]}.mp3` ],
        volume: s,
        onload: this.onSoundLoaded.bind(this)

      });

      sounds[manifest[i]] = sound;

    }

  }

  onSoundLoaded() {

    soundsLoaded++

    if ( soundsLoaded === manifest.length + 1 ) {
      console.log('sounds ready')
    }

  }

  play( soundName, pos = null ) {

    if ( pos ) sounds[ soundName ].pos( pos )
    sounds[ soundName ].play()

  }

  stop( soundName ) {

    sounds[soundName].pause();
    sounds[soundName].pos(0);

  }

  pause( soundName ) {

    sounds[soundName].pause();
  }

}

const _sm = new SoundManager()

export default _sm;