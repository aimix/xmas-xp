import GameController from 'controllers/app-controller';
import Projectile from 'entities/projectile';
import Canon from 'entities/canon';
import EventEmitter from 'lib/event-emitter'
import Santa from 'entities/characters/santa';

const poolSize = 20;

class ProjectilesManager {

  constructor() {
    this.projectiles = [];
    this.container = new THREE.Group()

    this.canon_left = new Canon();
    this.canon_left.rotation.y = 0;
    this.canon_left.position.z = 80;
    this.container.add(this.canon_left);

    this.canon_right = new Canon();
    this.canon_right.rotation.y = Math.PI;
    this.canon_right.position.z = -60;
    this.container.add(this.canon_right);
  }

  /**
   * Inits a `poolSize` projectiles pool once
   */
   initPool() {
    for ( let i = 0; i < poolSize; i++ ) {
      let projectile = new Projectile();
      this.projectiles.push( projectile );
      this.container.add(projectile);
      projectile.visible = false
    }
  }

  /**
   * Returns the first projet in this.projectile
   * @return {Projectile} projectile
   */
   getProjectileFromPool() {
    let projectile = this.projectiles.shift();
    projectile.visible = true;
    return projectile;
  }

  /**
   * Retuns a projectile to the pool for later use
   */
   returnProjectileToPool(projectile) {
    projectile.visible = false;
    this.projectiles.push(projectile);
  }

  /**
   * Add a projectile to the scene
   */
   throwProjectile() {

    let projectile = this.getProjectileFromPool()

    let startRotation = {x:0, y:0, z:0}
    let endRotation = {x:Math.random()*20-10, y:Math.random()*20-10, z:Math.random()*20-10}

    let side = Math.random()>0.5 ? 1 : -1;

    let currentCanon = "canon_left";
    let canonAngle = 0;
    if (side==-1) {
      currentCanon = "canon_right";
      canonAngle = -Math.PI;
    };

    let startPosition = {x:this[currentCanon].position.x, y:this[currentCanon].position.y, z:this[currentCanon].position.z}

    let currentTargetPosition = Santa.container.position.clone();
    currentTargetPosition.x-=20*Santa.speed;
    let dx = currentTargetPosition.x-startPosition.x
    let dy = currentTargetPosition.x-startPosition.y
    let dz = currentTargetPosition.z-startPosition.z
    let distance = Math.pow(dx*dx+dz*dz,0.5)
    let angle = Math.atan2(dz, dx)

    TweenMax.set(projectile.position,startPosition)
    TweenMax.set(projectile.scale,{x:15,y:15,z:15})
    TweenMax.set(projectile.rotation,startRotation)

    TweenMax.killTweensOf(this[currentCanon].rotation);
    TweenMax.set(this[currentCanon].material.uniforms.displacement,{value:0.0});
    TweenMax.to(this[currentCanon].material.uniforms.displacement,1.0,{delay:.05, ease:Expo.easeInOut, value:1.0});
    TweenMax.to(this[currentCanon].rotation,1.7,{delay:1, ease:Power1.easeInOut, y:canonAngle});
    TweenMax.to(this[currentCanon].rotation,.35,{delay:0, ease:Back.easeOut, y:-angle-Math.PI*.5, onComplete:()=>{

      currentTargetPosition = Santa.container.position.clone();
      currentTargetPosition.x-=20*Santa.speed;
      dx = currentTargetPosition.x-startPosition.x
      dy = currentTargetPosition.x-startPosition.y
      dz = currentTargetPosition.z-startPosition.z
      distance = Math.pow(dx*dx+dz*dz,0.5)
      angle = Math.atan2(dz, dx)

      let targetPostion = {x:startPosition.x+distance*Math.cos(angle), z:startPosition.z+distance*Math.sin(angle)+side*5, y:10};
      let endAngle = Math.random()-.5;
      endAngle = -angle+side*Math.random()*1.0
      let endDistance = distance*0.3
      let endPostion = {x:targetPostion.x+endDistance*Math.cos(endAngle), y:currentTargetPosition.y-40-0*Math.random()*30, z:targetPostion.z+endDistance*Math.sin(endAngle)}

      TweenMax.set(projectile.material,{opacity:1, transparent:true})
      let delayShoot = 0.2
      TweenMax.to(projectile.scale,.2,{delay:delayShoot, ease:Linear.easeNone, x:25, y:25, z:25})
      TweenMax.to(projectile.rotation,.7,{delay:delayShoot, ease:Linear.easeNone, x:endRotation.x, y:endRotation.y, z:endRotation.z})
      TweenMax.to(projectile.position,.7,{delay:delayShoot, ease:Power0.easeIn, x:targetPostion.x, y:targetPostion.y, z:targetPostion.z, onComplete:()=>{
        this.side = side;
        EventEmitter.emit( 'PROJECTILE_ON_TARGET', side );
        TweenMax.to(projectile.rotation,1.4,{ease:Linear.easeNone, x:startRotation.x*2, y:startRotation.y*2, z:startRotation.z*2})
        TweenMax.to(projectile.position,1.4,{ease:Power2.easeOut, x:endPostion.x, z:endPostion.z})
        TweenMax.to(projectile.material,0.7,{ease:Expo.easeIn, opacity:0})
        TweenMax.to(projectile.position,.7,{ease:Back.easeIn, y:endPostion.y, onComplete:()=>{
          TweenMax.to(projectile.position,.7,{ease:Linear.easeNone, y:endPostion.y-160, onComplete:()=>{
            this.returnProjectileToPool(projectile);
          }})
        }})
      }})
    }});
  }


  transitionOutCanons() {
    this.canon_left.transitionOut()
    this.canon_right.transitionOut()
  }

  /**
   * handle throwing according to delta time and interval
   * @param  {Number} dt
   */
   update( dt ) {

  }

}

const _pm = new ProjectilesManager();
export default _pm;