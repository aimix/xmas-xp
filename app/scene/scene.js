/*
scene.js
 */
import GUIController from 'utils/gui-controller';
const OrbitControls = require('three-orbit-controls')(THREE);

let effectFXAA = new THREE.ShaderPass( THREE.FXAAShader );
class Scene {

  /**
   * @constructor
   */
  constructor() {


  }

  init() {

    this.width = window.innerWidth
    this.height = window.innerHeight

    this.cameraZoom = 2.3
    this.cameraFov = 2.3
    this.cameraPX = 2.3
    this.cameraPY = 2.3
    this.cameraPZ = 2.3

    this.target = new THREE.Vector3( 0, 0, 0 );
    

    this.renderer = new THREE.WebGLRenderer( this.width, this.height, { antialias: true } )

    this.camera = new THREE.PerspectiveCamera( 45, this.width / this.height, 1, 20000 ) 
    this.camera.position.set( 0, 200, -300 )


    this.scene = new THREE.Scene()


    this.composer = new THREE.EffectComposer( this.renderer );
    this.composer.addPass( new THREE.RenderPass( this.scene, this.camera ) );


    let effectFilm = new THREE.FilmPass( 0.25, 0.025, 648, false );
    // effectFilm.renderToScreen = true;
    this.composer.addPass( effectFilm );
    
    effectFXAA.uniforms[ 'resolution' ].value.set( 1 / this.width, 1 / this.height );
    effectFXAA.renderToScreen = true;
    this.composer.addPass( effectFXAA );
    
    this.composer.setSize( this.width, this.height )


    this.renderer.setSize( this.width, this.height )



    this.controls = null;
    // this.initControls()

    this.camera.lookAt(this.target);
    this.camera.zoom = 1;
    this.camera.updateProjectionMatrix();
    this.scene.add( new THREE.AmbientLight( 0xffffff ) );

    window.camera = this.camera

    // this.initGUIOptions()
        
  } 

  initGUIOptions() {

    let cameraFolder = GUIController.gui.addFolder( 'Camera' );

    cameraFolder.add( this, 'cameraFov', 0.5, 200 ).onChange( () => { this.camera.fov = this.cameraFov; this.camera.updateProjectionMatrix(); } )
    cameraFolder.add( this, 'cameraZoom', 0.15, 13 ).onChange( () => { this.camera.zoom = this.cameraZoom; this.camera.updateProjectionMatrix(); } )
    cameraFolder.add( this, 'cameraPX', -500, 500 ).onChange( () => { this.camera.position.x = this.cameraPX; } )
    cameraFolder.add( this, 'cameraPY', -500, 500 ).onChange( () => { this.camera.position.y = this.cameraPY; } )
    cameraFolder.add( this, 'cameraPZ', -500, 500 ).onChange( () => { this.camera.position.z = this.cameraPZ; } )

  }

  shake() {

    // let o = { time: 0 }
    // TweenMax.to( o, 1, { time: 2, onUpdate: () => {
    //   let r = Math.random()
    //   this.camera.position.x += r * 10
    //   this.camera.position.y += r 
    // } } )

  }

  /**
   * Orbit controls
   */
  initControls() {
    
    this.controls = new OrbitControls(this.camera);


  }

  /**
   * Add a child to the scene
   *
   * @param {Obj} child - a THREE object
   */
  add( child ) {

    this.scene.add( child )

  }

  /**
   * Remove a child from the scene
   *
   * @param {Obj} child - a THREE object
   */
  remove( child ) {

    this.scene.remove( child )

  }

  /**
   * Renders/Draw the scene
   */
  render() {

    this.camera.lookAt(this.target);
    
    if (this.composer){
      this.composer.render(Math.random())
    } else{
      this.renderer.render( this.scene, this.camera );
    }

  }

  /**
   * Resize the scene according to screen size
   *
   * @param {Number} newWidth
   * @param {Number} newHeight
   */
  resize( newWidth, newHeight ) {
    this.camera.aspect = newWidth / newHeight
    this.camera.updateProjectionMatrix()

    this.renderer.setSize( newWidth, newHeight )
    if (this.composer) {
      this.composer.setSize( newWidth, newHeight )
      effectFXAA.uniforms[ 'resolution' ].value.set( 1 / newWidth, 1 / newHeight );
    };

  }

}

const _scene = new Scene()

export default _scene
